package com.jxjiang.chatapplication.client;

import android.os.Handler;
import android.os.Looper;

import java.io.IOException;

import top.xx252016.coreClient.base.Contract;
import top.xx252016.coreClient.clientChat.ChatClientMain;

public class ChatClientAndroid implements Contract.SubThreadContract {
    private Contract.AppContract mContract;
    private Contract.AppContract subContract;
    Handler mHandler;
    ChatClientMain clientMain=ChatClientMain.getInstance();

    public ChatClientAndroid(final Contract.AppContract contract){
        mHandler = new Handler(Looper.getMainLooper());
        subContract = contract;
        mContract = new Contract.AppContract() {
            @Override
            public void readFromSocket(final String str) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(subContract != null)
                            subContract.readFromSocket(str);
                    }
                });
            }
        };
        clientMain.setAppContract(mContract);

    }

    public void setSubContract(Contract.AppContract contract){
        subContract = contract;
    }

    @Override
    public void start() {
        clientMain.clientMain();
        clientMain.changeTrml("Android");
        clientMain.doJoin();
    }

    @Override
    public void fecth2Socket(String string) {
        try {
            clientMain.send2Socket(string);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void quit() {
        clientMain.quit();
    }
}

