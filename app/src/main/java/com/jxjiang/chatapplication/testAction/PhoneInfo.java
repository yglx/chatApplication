package com.jxjiang.chatapplication.testAction;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Button;

import java.util.List;

/**
 * Created by Y_X_Y on 2016/12/6.
 */
public class PhoneInfo extends AppCompatActivity {

    private List<PhoneInfo> lists;
    private Button mButton;

    public void onClick() {
        //得到ContentResolver对象
        ContentResolver cr = getContentResolver();
        //取得电话本中开始一项的光标
        Cursor cursor = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        //向下移动光标
        while (cursor.moveToNext()) {
            //取得联系人名字
            int nameFieldColumnIndex = cursor.getColumnIndex(ContactsContract.PhoneLookup.DISPLAY_NAME);
            String contact = cursor.getString(nameFieldColumnIndex);
            //取得电话号码
            String ContactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
            Cursor phone = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + ContactId, null, null);

            while (phone.moveToNext()) {
                String PhoneNumber = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                //String PhoneMsg1   = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE));
                //String PhoneMsg2   = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE));
                //String PhoneMsg3   = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.EXTRA_ADDRESS_BOOK_INDEX));
                //String PhoneMsg4   = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.EXTRA_ADDRESS_BOOK_INDEX_COUNTS));
                //String PhoneMsg5   = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.EXTRA_ADDRESS_BOOK_INDEX_TITLES));
                //String PhoneMsg6   = phone.getString(phone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NORMALIZED_NUMBER));
                //格式化手机号
                PhoneNumber = PhoneNumber.replace("-", "");
                PhoneNumber = PhoneNumber.replace(" ", "");
                Log.d("jdj", "onCreate: "+PhoneNumber);
                //Log.d("jdj", "onCreate: "+PhoneNumber+","+PhoneMsg1+","+PhoneMsg2+","+PhoneMsg3+","+PhoneMsg4+","+PhoneMsg5+","+PhoneMsg6);
                //Log.d("jdj", "onCreate: "+PhoneMsg2);
            }
        }
        Log.d("jdj", "End");
    }
}
