package com.jxjiang.chatapplication.testAction;

import android.content.Context;
import android.content.Intent;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ApnQuick {

	public void startAnp(Context context) {
		Intent intent = new Intent(Settings.ACTION_APN_SETTINGS);
		//get MTK SIM subid if it's MTK platform
		try {
			Class<?> MtkSubscriptionManager = Class.forName("android.telephony.SubscriptionManager");
			Constructor MtkSubscriptionManagerConstructor = MtkSubscriptionManager.getConstructor(Context.class);
			Object MtkSubscriptionManagerObject = MtkSubscriptionManagerConstructor.newInstance(this);

			Method getDefaultSubId = MtkSubscriptionManager.getMethod("getDefaultSubId", new Class[0]);
			int subid = (int) getDefaultSubId.invoke(MtkSubscriptionManagerObject, new Object[]{});

			Log.d("jdj", subid +"");
			intent.putExtra("sub_id", subid);
			System.out.println("11111111111111111111111111111111111111");
			//Toast.makeText(getApplicationContext(), "11111111111111111111111111111111111111", Toast.LENGTH_SHORT).show();
		} catch (ClassNotFoundException e) {
			Log.d("jdj", "ClassNotFoundException");
		} catch (NoSuchMethodException e) {
			Log.d("jdj", "NoSuchMethodException");
		} catch (InstantiationException e) {
			Log.d("jdj", "InstantiationException");
		} catch (IllegalAccessException e) {
			Log.d("jdj", "IllegalAccessException");
		} catch (IllegalArgumentException e) {
			Log.d("jdj", "IllegalArgumentException");
		} catch (InvocationTargetException e) {
			Log.d("jdj", "InvocationTargetException");
		}finally{
			//start acivity anyway
			context.startActivity(intent);
			System.out.println("ApnQuick");
			Toast.makeText(context, "ApnQuick", Toast.LENGTH_SHORT).show();
			//finish();
		}
	}
}
