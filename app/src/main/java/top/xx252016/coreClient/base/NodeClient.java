package top.xx252016.coreClient.base;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public final class NodeClient {
    private Socket serverSocket;
    public ObjectOutputStream oos;
    public ObjectInputStream ois;

    private String username;
    private String password;
    /* 昵称 */
    private String nickName;
    private String niclkype;
    private String nickShot;
    /* 登录相关> .上线时间.下线时间.登录状态.登录终端 *//*登录次数（其他）*/
    private String LoginB;
    private String LoginE;
    private String LoginS;
    private String LoginT;
    /* 终端属性 */
    private String clientIP;
    private String clientCharset;
    private String serverCharset;

    public Socket getServerSocket() {
        return serverSocket;
    }

    public void setServerSocket(Socket serverSocket) {
        this.serverSocket = serverSocket;
    }

    public ObjectOutputStream getOos() {
        return oos;
    }

    public void setOos(ObjectOutputStream oos) {
        this.oos = oos;
    }

    public ObjectInputStream getOis() {
        return ois;
    }

    public void setOis(ObjectInputStream ois) {
        this.ois = ois;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickType() {
        return niclkype;
    }

    public void setNickType(String niclType) {
        this.niclkype = niclType;
    }

    public String getNickShot() {
        return nickShot;
    }

    public void setNickShot(String nickShot) {
        this.nickShot = nickShot;
    }

    public String getLoginB() {
        return LoginB;
    }

    public void setLoginB(String loginB) {
        LoginB = loginB;
    }

    public String getLoginE() {
        return LoginE;
    }

    public void setLoginE(String loginE) {
        LoginE = loginE;
    }

    public String getLoginS() {
        return LoginS;
    }

    public void setLoginS(String loginS) {
        LoginS = loginS;
    }

    public String getLoginT() {
        return LoginT;
    }

    public void setLoginT(String loginT) {
        LoginT = loginT;
    }

    public String getClientIP() {
        return clientIP;
    }

    public void setClientIP(String clientIP) {
        this.clientIP = clientIP;
    }

    public String getClientCharset() {
        return clientCharset;
    }

    public void setClientCharset(String clientCharset) {
        this.clientCharset = clientCharset;
    }

    public String getServerCharset() {
        return serverCharset;
    }

    public void setServerCharset(String serverCharset) {
        this.serverCharset = serverCharset;
    }

}
