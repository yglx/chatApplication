package top.xx252016.coreClient.base;

/**
 * Created by jiangwan on 2018/7/10.
 */

public interface Contract {
    interface AppContract {
        //从服务器获取消息
        void readFromSocket(String str);
    }

    interface SubThreadContract {
        //启动服务
        void start();

        //读数据到服务器
        void fecth2Socket(String string);

        //关闭服务socket
        void quit();

    }
}
